import login from '@/api/login';
import user from "@/api/user";
import role from "@/api/role";
import dept from "@/api/dept";
import menu from "@/api/menu";
import init from "@/api/init";

export default {
    login,
    user,
    role,
    dept,
    menu,
    init
}
