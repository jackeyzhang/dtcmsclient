import { axios } from '@/utils/request'

const role = {

    // 添加角色
    saveOrUpdate(parameter,flag) {
        if(flag === 0){
            return axios({
                url: '/api/v1/role/save',
                method: 'post',
                data: parameter
            })
        }else {
            return axios({
                url: '/api/v1/role/update',
                method: 'put',
                data: parameter
            })
        }
    },
    // 获取角色列表
    list(parameter) {
        return axios({
            url: '/api/v1/role/list',
            method: 'get',
            params: parameter
        })
    },
    // 获取角色信息
    getById(id) {
        return axios({
            url: '/api/v1/role/getById/'+id,
            method: 'get'
        })
    },
    // 删除角色
    remove(id) {
        return axios({
            url: '/api/v1/role/delete/'+id,
            method: 'delete'
        })
    },
    // 获取全部角色
    findAll() {
        return axios({
            url: '/api/v1/role/findAll',
            method: 'get'
        })
    }
}

export default role;
