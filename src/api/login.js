import { axios } from '@/utils/request'

const login = {

    // 登录系统接口
    sysLogin(data) {
        return axios({
            url: '/api/user/login',
            method: 'post',
            data
        })
    },
    // 退出系统接口
    sysLogout() {
        return axios({
            url: '/api/user/loginOut',
            method: 'post'
        })
    }
}

export default login;
