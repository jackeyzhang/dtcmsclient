const CryptoJS = require('crypto-js');
// 十六位十六进制数作为密钥（KEY必须和后台保持一致）
const key = CryptoJS.enc.Utf8.parse("CMDDTYDF&WY196KJ");
// 十六位十六进制数作为密钥偏移量
const iv = CryptoJS.enc.Utf8.parse('CMDDTYDF&WY196KJ');

//解密方法
function Decrypt(data) {
    let encryptedHexStr = CryptoJS.enc.Base64.parse(data);
    let srcs = CryptoJS.enc.Base64.stringify(encryptedHexStr);
    let decrypt = CryptoJS.AES.decrypt(srcs, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
    return decryptedStr.toString();
}

//加密方法
function Encrypt(data) {
    let srcs = CryptoJS.enc.Utf8.parse(data);
    let encrypted = CryptoJS.AES.encrypt(srcs, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
}

export default {
    Decrypt ,
    Encrypt
}
