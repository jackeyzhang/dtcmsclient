import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/common.css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Fragment from 'vue-fragment'
import permissions from './permissions/index';
import api from './api';
import Storage from 'vue-ls';
import 'font-awesome/css/font-awesome.min.css'
import aes from './utils/crypto-aes'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
Vue.prototype.cryptoAes = aes
Vue.prototype.hasPerm = permissions;
Vue.prototype.$api = api;
Vue.use(Storage);
Vue.use(Fragment.Plugin)
Vue.use(ElementUI);
Vue.config.productionTip = false
// 进入路由之前执行
router.beforeEach((to, from, next) => {
    // 请求进度条效果
    NProgress.start()
    // 1、获取当前打开的选项卡
    store.commit('getTabs');
    // 2、设置当前激活的选项卡
    store.commit('setActiveTabs', to.name);
    // 3、检查是否存在token
    let token = localStorage.getItem('token');
    if(to.path === '/admin/login') {
        if(token) {
            next({path: '/admin/home'})
        }else{
            next();
        }
    }else {
        if(!token && to.name !== 'login') {
            next({path: '/admin/login'})
        }else {
            if(store.state.MenuStore.menu_data.length === 0) {
                store.commit('getMenuList',router);
                if(to.path === "/") {
                    next({path:'/admin/home'})
                }else {
                    next({path:to.path});
                }
            }else {
                next();
            }
        }
    }
})
router.afterEach(() => {
    NProgress.done()
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
