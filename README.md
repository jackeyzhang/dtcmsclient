### DT CMS 平台

 **Spring Boot前后端分离架构，大道至简、代码玄学、开箱即用**  <br>

![输入图片说明](https://img.shields.io/badge/spring--boot-2.3.5-green.svg "在这里输入图片标题") ![输入图片说明](https://img.shields.io/badge/Vue-3.5.0-brightgreen "在这里输入图片标题") ![输入图片说明](https://img.shields.io/badge/redis-6.0.6-brightgreen "在这里输入图片标题") ![输入图片说明](https://img.shields.io/badge/kaptcha-2.3.2-brightgreen "在这里输入图片标题") ![输入图片说明](https://img.shields.io/badge/MySQL-8.0.81-brightgreen "在这里输入图片标题") ![输入图片说明](https://img.shields.io/badge/mybatisplus-3.4.6-brightgreen "在这里输入图片标题") ![输入图片说明](https://img.shields.io/badge/easyexcel-2.2.0beta2-brightgreen "在这里输入图片标题")


# 平台简介

 **DT CMS**  是一款SpringBoot2.x与Vue整合前后端分离内容管理系统，项目源码完全由个人精心编写，致力于做更简洁高性能RBAC内容服务系统，追求 **快速的用户体验** 、 **二次编码** ，以及 **核心技术模块的整合** 使用。后端新技术框架的加持、前端UI的设计与美化，会持续升级，持续完善，欢迎亲友们收藏、点赞和转发。

SpringBoot后端链接：[Service端入口](https://gitee.com/summerydf/dtcmsservice)

线上体验地址：[加入CMS基础版本体验](http://47.108.191.196)，账号密码：root/root123 ，注：请大家不要乱删除数据，影响体验效果。

## 平台优势

一款极易扩展的开源系统，可用于学习、商用、二次开发等等，系统配套了开发使用手册，部署手册，以及开发说明手册，并且整合了最新潮流的技术框架，源代码注释完整、结构简洁、结构清晰、便维护、便迭代。


## 核心技术

SpringBoot、MybatisPlus、SpringSecurity、JWT令牌使用RSA秘钥非对称加密，极大限度保证系统安全性，数据库采用MySQL、Redis，文件服务器：Minio,前端UI：Vue、ElementUI



## 项目安装
```
npm install
```

### 项目运行
```
npm run serve
```

### 项目打包
```
npm run build
```
## 界面效果


<table>
    <tr>
        <td><img src="https://img-blog.csdnimg.cn/2bd87933bf9a4ab3bb737fec732f93a2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBARFTovrDnmb0=,size_20,color_FFFFFF,t_70,g_se,x_16"></img></td>
        <td><img src="https://img-blog.csdnimg.cn/b48cfd9d8c6041ddac1559b337c75207.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBARFTovrDnmb0=,size_20,color_FFFFFF,t_70,g_se,x_16"></img></td>
    </tr>

<tr>
        <td><img src="https://img-blog.csdnimg.cn/c075f6b2fbde42709cb497137bf0c1a4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBARFTovrDnmb0=,size_20,color_FFFFFF,t_70,g_se,x_16"></img></td>
        <td><img src="https://img-blog.csdnimg.cn/a0f6f4086c884c6c9749132e1ef3ac61.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBARFTovrDnmb0=,size_20,color_FFFFFF,t_70,g_se,x_16"></img></td>
    </tr>

<tr>
        <td><img src="https://img-blog.csdnimg.cn/5a55bb5681784ff6a110214853d61a5f.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBARFTovrDnmb0=,size_20,color_FFFFFF,t_70,g_se,x_16"></img></td>
        <td><img src="https://img-blog.csdnimg.cn/ba445aa03b8a420eb71545fd8a48c80c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBARFTovrDnmb0=,size_20,color_FFFFFF,t_70,g_se,x_16"></img></td>
    </tr>

<tr>
        <td><img src="https://img-blog.csdnimg.cn/cd68e6040d1941308c9a45cd09050048.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBARFTovrDnmb0=,size_20,color_FFFFFF,t_70,g_se,x_16"></img></td>
        <td><img src="https://img-blog.csdnimg.cn/85e3a7f884be4916815cba8af116e52f.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBARFTovrDnmb0=,size_20,color_FFFFFF,t_70,g_se,x_16"></img></td>
    </tr>
  
</table>

## 联系我们

1973984292@qq.com